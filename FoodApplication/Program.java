import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        // Объявляю объект для считывания чисел с консоли
        Scanner scanner = new Scanner(System.in);
        // Считываю название еды
        String foodName = scanner.nextLine();
        // Вывожу на экран
        System.out.println("FOOD NAME: " + foodName);
    }
}
